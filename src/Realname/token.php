<?php

namespace JyRealnameBd\Realname;

use JyRealnameBd\Kernel\Http;

trait token
{
  /**
   * http://composer.ijingyi.com/demo-realname-bd.php?a=getAccessToken
   * @return mixed
   */
  public function getAccessToken()
  {
    $param = [
      'grant_type'    => 'client_credentials',
      'client_id'     => $this->config['api_key'],
      'client_secret' => $this->config['secret_key'],
    ];
    return Http::httpPostJson($this->domainUrl . '/oauth/2.0/token?' . http_build_query($param));
  }
  
  public function setAccessToken($access_token)
  {
    $this->access_token = $access_token;
  }
  
  /**
   * 取verify_token(有效期为2小时)
   * https://ai.baidu.com/ai-doc/FACE/aknovy94h#1%E8%8E%B7%E5%8F%96verify_token%E6%8E%A5%E5%8F%A3
   *
   * @param string $plan_id 方案的id信息，请在人脸实名认证控制台查看创建的H5方案的方案ID信息
   * @return void
   */
  public function getVerifyToken($plan_id)
  {
    $param = [
      'plan_id' => $plan_id,
    ];
    return Http::httpPostJson($this->domainUrl . '/rpc/2.0/brain/solution/faceprint/verifyToken/generate?access_token=' . $this->access_token, $param);
  }
}
