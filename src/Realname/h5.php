<?php

namespace JyRealnameBd\Realname;

use JyRealnameBd\Kernel\Http;

trait h5
{
  /**
   * 创建H5认证URL
   * https://ai.baidu.com/ai-doc/FACE/skxie72kp
   *
   * @param $verify_token
   * @param $successUrl
   * @param $failedUrl
   * @return string
   */
  public function createH5Url($verify_token, $successUrl, $failedUrl = '')
  {
    $param = [
      'token'      => $verify_token,
      'successUrl' => $successUrl,
    ];
    if (!isset($failedUrl) || $failedUrl == '') {
      $param['failedUrl'] = $param['successUrl'] . '&fail=1';
    } else {
      $param['failedUrl'] = $failedUrl;
    }
    return 'https://brain.baidu.com/face/print/?' . http_build_query($param);
  }
  
  /**
   * 取认证人脸接口
   * https://ai.baidu.com/ai-doc/FACE/aknovy94h#1%E8%8E%B7%E5%8F%96%E8%AE%A4%E8%AF%81%E4%BA%BA%E8%84%B8%E6%8E%A5%E5%8F%A3
   *
   * @param string $verify_token
   * @return mixed
   */
  public function getResultSimple($verify_token)
  {
    $param = [
      'verify_token' => $verify_token,
    ];
    return Http::httpPostJson($this->domainUrl . '/rpc/2.0/brain/solution/faceprint/result/simple?access_token=' . $this->access_token, $param);
  }
  
  /**
   * 查询认证结果接口
   * https://ai.baidu.com/ai-doc/FACE/aknovy94h#2%E6%9F%A5%E8%AF%A2%E8%AE%A4%E8%AF%81%E7%BB%93%E6%9E%9C%E6%8E%A5%E5%8F%A3
   *
   * @param string $verify_token
   * @return mixed
   */
  public function getResultDetail($verify_token)
  {
    $param = [
      'verify_token' => $verify_token,
    ];
    return Http::httpPostJson($this->domainUrl . '/rpc/2.0/brain/solution/faceprint/result/detail?access_token=' . $this->access_token, $param);
  }
  
  /**
   * 查询统计结果
   * https://ai.baidu.com/ai-doc/FACE/aknovy94h#3%E6%9F%A5%E8%AF%A2%E7%BB%9F%E8%AE%A1%E7%BB%93%E6%9E%9C
   *
   * @param string $verify_token
   * @return mixed
   */
  public function getResulStat($verify_token)
  {
    $param = [
      'verify_token' => $verify_token,
    ];
    return Http::httpPostJson($this->domainUrl . '/rpc/2.0/brain/solution/faceprint/result/stat?access_token=' . $this->access_token, $param);
  }
  
  /**
   * 指定用户信息上报
   * https://ai.baidu.com/ai-doc/FACE/wknovzoxe#2%E6%8C%87%E5%AE%9A%E7%94%A8%E6%88%B7%E4%BF%A1%E6%81%AF%E4%B8%8A%E6%8A%A5%E6%8E%A5%E5%8F%A3
   *
   * @param string $verify_token
   * @param array  $param
   * @return mixed
   */
  public function idcardInput($verify_token, $param = [])
  {
    $param = [
      'verify_token'     => $verify_token,
      'id_name'          => $param['id_name'],
      'id_no'            => $param['id_no'],
      'certificate_type' => (int)$param['certificate_type'],
    ];
    return Http::httpPostJson('https://brain.baidu.com/solution/faceprint/idcard/submit', $param);
  }
}
