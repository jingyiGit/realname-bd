<?php

namespace JyRealnameBd\Realname;

class Realname
{
  use token;
  use h5;
  
  protected $domainUrl = 'https://aip.baidubce.com';
  protected $config = [];
  protected $error = null;
  protected $access_token = '';
  
  public function __construct($config)
  {
    $this->config = $config;
  }
  
  private function handleReturn($res)
  {
    if ($res['code'] == 200) {
      $this->error = null;
      return $res;
    }
    $this->setError($res);
    return false;
  }
  
  public function getError()
  {
    return $this->error;
  }
  
  private function setError($error)
  {
    $this->error = $error;
  }
}
