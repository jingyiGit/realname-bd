<?php

namespace JyRealnameBd\BasicService;

trait BaseConfig
{
  /**
   * 初始化配置
   *
   * @param array $config 配置
   * @return array
   */
  protected function initConfig($config)
  {
    // 检测必要配置项
    $this->checkConfig($config);
    return $config;
  }
  
  /**
   * 检测必要配置项
   *
   * @param $config
   */
  protected function checkConfig($config)
  {
    if (!isset($config['api_key']) || !$config['api_key']) {
      $this->fail('api_key 不能为空');
    } else if (!isset($config['secret_key']) || strlen(trim($config['secret_key'])) != 32) {
      $this->fail('secret_key 为空或长度不对');
    }
  }
}
