<?php

namespace JyRealnameBd\Init;

use JyRealnameBd\Kernel\Response;
use JyRealnameBd\Realname\Realname;
use JyRealnameBd\BasicService\BaseConfig;

/**
 * Class Application.
 */
class Application extends Realname
{
  use Response;
  use BaseConfig;
  
  public function __construct($config = [])
  {
    parent::__construct($this->initConfig($config));
  }
}
